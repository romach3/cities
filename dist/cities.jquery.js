(function ( $ ) {
    $.fn.cities = function( options ) {
        var settings = $.extend({
            "modal-id": "data-cities-modal",
            "cities": undefined,
            "json": "../../dist/cities.json",
            "selectCityCallback": function (item) {
                alert(item.name);
            }
        }, options );
        
        $.getJSON(settings.json, function (response) {
            JSX.data = response;
            if (settings.cities !== undefined) {
                JSX.initAllCities();
                JSX.fillExists();
            }
            JSX.initDistricts(1);
        });

        var runSelectCityCallback = function (item) {
            item = JSX.data.all_cities[item.name];
            item.district_name = JSX.data.districts[item.country_id][item.district_id].name;
            item.region_name = JSX.data.regions[item.country_id][item.district_id][item.region_id].name;
            settings.selectCityCallback(item);
        };

        var JSX = {
            data: [],
            clearCities: undefined,
            existsDistricts: [],
            existsRegions: [],
            fillExists: function () {
                if (settings.cities !== undefined) {
                    settings.cities.map(function (city) {
                        var item = this.data.all_cities[city];
                        if (this.existsDistricts.indexOf(item.district_id) < 0) {
                            this.existsDistricts.push(item.district_id.toString());
                        }
                        if (this.existsRegions.indexOf(item.region_id) < 0) {
                            this.existsRegions.push(item.region_id.toString());
                        }
                    }.bind(this))
                }
            },
            initAllCities: function () {
                var self = this;
                var changeDataCallbackSetter;
                var items = {};
                var $dataCitiesList = $('#data-cities-list');
                settings.cities.map(function (city) {
                    items[city] = this.data.all_cities[city];
                    $dataCitiesList.append('<option value="' + city + '">')
                }.bind(this));
                $('[data-cities-search]').on('input', function () {
                    var value = $(this).val();
                    if (value.length > 0) {
                        var newItems = {};
                        for (var id in items) {
                            if (items.hasOwnProperty(id)) {
                                if (id === value) {
                                    runSelectCityCallback(items[id]);
                                } else if (id.indexOf(value) > -1) {
                                    newItems[id] = items[id];
                                }
                            }
                        }
                    } else {
                        newItems = items;
                    }
                    changeDataCallbackSetter(newItems);
                });
                ListCreator().init({
                    container: $('[data-cities-ul="AllCities"]'),
                    items: items,
                    selectItemCallback: runSelectCityCallback,
                    changeDataCallbackSetter: function (callback) {
                        changeDataCallbackSetter = callback;
                    },
                    ulClass: "nav nav-pills"
                });
            },
            initDistricts: function (country_id) {
                var items = this.data.districts[country_id];
                if (settings.cities !== undefined) {
                    items = {};
                    for(var key in this.data.districts[country_id]) {
                        if (this.data.districts[country_id].hasOwnProperty(key)) {
                            if (this.existsDistricts.indexOf(key) > 0) {
                                items[key] = this.data.districts[country_id][key];
                            }
                        }
                    }
                }
                var selectItemCallback = function (item) {
                    if (this.clearCities !== undefined) {
                        this.clearCities();
                    }
                    changeDistrict(country_id, item.id);
                }.bind(this);
                ListCreator().init({
                    container: $('[data-cities-ul="Districts"]'),
                    items: items,
                    selectItemCallback: selectItemCallback,
                    changeDataCallbackSetter: function (callback) {

                    }
                });
                var changeDistrict = this.initRegions();
            },
            initRegions: function () {
                var changeDataCallbackSetter;
                var current_country_id = 0;
                var current_district_id = 0;
                var selectItemCallback = function (item) {
                    changeRegion(current_country_id, current_district_id, item.id);
                };
                ListCreator().init({
                    container: $('[data-cities-ul="Regions"]'),
                    items: [],
                    selectItemCallback: selectItemCallback,
                    changeDataCallbackSetter: function (callback) {
                        changeDataCallbackSetter = callback;
                    }
                });
                var changeRegion = this.initCities();
                return function (country_id, district_id) {
                    var items = this.data.regions[country_id][district_id];
                    current_country_id = country_id;
                    current_district_id = district_id;
                    if (settings.cities !== undefined) {
                        items = {};
                        for(var key in this.data.regions[country_id][district_id]) {
                            if (this.data.regions[country_id][district_id].hasOwnProperty(key)) {
                                if (this.existsRegions.indexOf(key) > 0) {
                                    items[key] = this.data.regions[country_id][district_id][key];
                                }
                            }
                        }
                    }
                    changeDataCallbackSetter(items);
                }.bind(this);
            },
            initCities: function () {
                var changeDataCallbackSetter;
                var current_country_id = 0;
                var current_district_id = 0;
                var current_region_id = 0;
                ListCreator().init({
                    container: $('[data-cities-ul="Cities"]'),
                    items: [],
                    selectItemCallback: runSelectCityCallback,
                    changeDataCallbackSetter: function (callback) {
                        changeDataCallbackSetter = callback;
                    }
                });
                this.clearCities = function () {
                    changeDataCallbackSetter({});
                };
                return function (country_id, district_id, region_id) {
                    var items = this.data.cities[country_id][district_id][region_id];
                    current_country_id = country_id;
                    current_district_id = district_id;
                    current_region_id = region_id;
                    if (settings.cities !== undefined) {
                        items = {};
                        this.data.cities[country_id][district_id][region_id].map(function (city) {
                            if (settings.cities.indexOf(city.id) > 0) {
                                items[city.id] = city;
                            }
                        });
                    }
                    changeDataCallbackSetter(items);
                }.bind(this);
            }
        };

        var ListCreator = function () {
            return {
                state: undefined,
                init: function (options) {
                    this.state = $.extend({
                        container: undefined,
                        changeDataCallbackSetter: function () {
                            
                        },
                        selectItemCallback: function () {

                        },
                        items: [],
                        ulClass: "nav nav-pills nav-stacked",
                        id: Math.floor(Math.random() * (999999 - 100000)) + 100000
                    }, options);
                    this.state.changeDataCallbackSetter(function (items) {
                        this.setState({
                            items: items
                        })
                    }.bind(this));
                    this.setState();
                },
                setState: function (state) {
                    this.state = $.extend(this.state, state);
                    this.render();
                },
                handleSelectItem: function (item) {
                    this.state.selectItemCallback(item);
                    $('ul[data-cities-ul="' + this.state.id + '"] li').removeClass('active');
                    $('ul[data-cities-ul="' + this.state.id + '"] li[data-key="' +  item.id + '"]').addClass('active');
                },
                render: function () {
                    var self = this;
                    var html = "";
                    var ids = [];
                    Object.keys(this.state.items).map(function (key) {
                        var item = this.state.items[key];
                        html += '<li data-key="' + key + '"><a href="#">' + item.name + '</a></li>';
                        ids.push(key);
                    }.bind(this));
                    html = '<ul class="' + this.state.ulClass + '" data-cities-ul="' + this.state.id + '">'
                        + html
                        + '</ul>';
                    this.state.container.html(html);
                    ids.map(function (id) {
                        this.state.container.find('li[data-key="' + id + '"]').click(function () {
                            self.state.selectItemCallback(self.state.items[id]);
                            $('ul[data-cities-ul="' + self.state.id + '"] li').removeClass('active');
                            $('ul[data-cities-ul="' + self.state.id + '"] li[data-key="' +  id + '"]').addClass('active');
                        });
                    }.bind(this));
                }
            }
        };
        
        return this;
    };
}( jQuery ));