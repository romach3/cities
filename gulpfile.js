var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');

var path = {
    js: ['./src/js/cities.jquery.js']
};

gulp.task('default', ['cities.min.js']);

gulp.task('cities.min.js', function () {
    gulp.src(path.js)
        .pipe(concat('cities.jquery.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(rename({suffix: ".min"}))
        .pipe(uglify())
        .pipe(gulp.dest('./dist'));
});

gulp.task('watch', function() {
    gulp.watch(path.js, ['cities.min.js']);
});