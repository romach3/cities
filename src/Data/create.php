<?php namespace Romach3\Cities\Data;

$data = getCityData();
$response = [
    'countries' => [],
    'districts' => [],
    'regions' => [],
    'cities' => [],
    'all_cities' => []
];

foreach ($data['world'] as $countryId => $country) {
    $response['countries'][] = [
        'id' => $countryId,
        'name' => $country['name'],
        'code' => $country['code']
    ];
    foreach ($data['cities'][$countryId] as $cityName => $cityData) {
        $response['cities'][$countryId][$cityData['federal_district']][$cityData['region']][] = [
            'name' => $cityName,
            'id' => $cityName,
            'timezone' => $cityData['timezone']
        ];
        $response['districts'][$countryId][$cityData['federal_district']] = [
            'id' => $cityData['federal_district'],
            'name' => $country['federal_district'][$cityData['federal_district']]
        ];
        $response['regions'][$countryId][$cityData['federal_district']][$cityData['region']] = [
            'id' => $cityData['region'],
            'name' => $country['region'][$cityData['region']]
        ];
        $response['all_cities'][$cityName] = [
            'name' => $cityName,
            'id' => $cityName,
            'timezone' => $cityData['timezone'],
            'country_id' => $countryId,
            'district_id' => $cityData['federal_district'],
            'region_id' => $cityData['region']
        ];
    }
}
file_put_contents('cities.json', json_encode($response, JSON_UNESCAPED_UNICODE));

/**
 * Возвращает массив данных
 * @return array
 */
function getCityData() {
    $base_world = [];
    $base_country_city = [];
    include "base_country_city.php";
    return [
        'world' => $base_world,
        'cities' => $base_country_city
    ];
}