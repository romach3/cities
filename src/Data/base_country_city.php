<?php
$base_world = array(
	//
	//
	// 	`7MM"""Mq.`7MMF'   `7MF'.M"""bgd  .M"""bgd `7MMF'      db
	// 	  MM   `MM. MM       M ,MI    "Y ,MI    "Y   MM       ;MM:
	// 	  MM   ,M9  MM       M `MMb.     `MMb.       MM      ,V^MM.
	// 	  MMmmdM9   MM       M   `YMMNq.   `YMMNq.   MM     ,M  `MM
	// 	  MM  YM.   MM       M .     `MM .     `MM   MM     AbmmmqMA
	// 	  MM   `Mb. YM.     ,M Mb     dM Mb     dM   MM    A'     VML
	// 	.JMML. .JMM. `bmmmmd"' P"Ybmmd"  P"Ybmmd"  .JMML..AMA.   .AMMA.
	//
	//
	'1' => array(
		'name' => 'Россия',
		'code' => 'ru',
		'federal_district' => array(
			1 => 'Центральный ФО',
			2 => 'Южный ФО',
			3 => 'Северо-Западный ФО',
			4 => 'Дальневосточный ФО',
			5 => 'Сибирский ФО',
			6 => 'Уральский ФО',
			7 => 'Приволжский ФО',
			8 => 'Северо-Кавказский ФО',
			9 => 'Крымский ФО',
		),
		'region' => array(
			1 => 'Москва',
			2 => 'Санкт-Петербург',
			3 => 'Севастополь',
			4 => 'Алтайский край',
			5 => 'Забайкальский край',
			6 => 'Камчатский край',
			7 => 'Краснодарский край',
			8 => 'Красноярский край',
			9 => 'Пермский край',
			10 => 'Приморский край',
			11 => 'Ставропольский край',
			12 => 'Хабаровский край',
			13 => 'Амурская область',
			14 => 'Архангельская область',
			15 => 'Астраханская область',
			16 => 'Белгородская область',
			17 => 'Брянская область',
			18 => 'Владимирская область',
			19 => 'Волгоградская область',
			20 => 'Вологодская область',
			21 => 'Воронежская область',
			22 => 'Ивановская область',
			23 => 'Иркутская область',
			24 => 'Калининградская область',
			25 => 'Калужская область',
			26 => 'Кемеровская область',
			27 => 'Кировская область',
			28 => 'Костромская область',
			29 => 'Курганская область',
			30 => 'Курская область',
			31 => 'Ленинградская область',
			32 => 'Липецкая область',
			33 => 'Магаданская область',
			34 => 'Московская область',
			35 => 'Мурманская область',
			36 => 'Нижегородская область',
			37 => 'Новгородская область',
			38 => 'Новосибирская область',
			39 => 'Омская область',
			40 => 'Оренбургская область',
			41 => 'Орловская область',
			42 => 'Пензенская область',
			43 => 'Псковская область',
			44 => 'Ростовская область',
			45 => 'Рязанская область',
			46 => 'Самарская область',
			47 => 'Саратовская область',
			48 => 'Сахалинская область',
			49 => 'Свердловская область',
			50 => 'Смоленская область',
			51 => 'Тамбовская область',
			52 => 'Тверская область',
			53 => 'Томская область',
			54 => 'Тульская область',
			55 => 'Тюменская область',
			56 => 'Ульяновская область',
			57 => 'Челябинская область',
			58 => 'Ярославская область',
			59 => 'Республика Адыгея',
			60 => 'Республика Алтай',
			61 => 'Республика Башкортостан',
			62 => 'Республика Бурятия',
			63 => 'Республика Дагестан',
			64 => 'Республика Ингушетия',
			65 => 'Республика Кабардино-Балкария',
			66 => 'Республика Калмыкия',
			67 => 'Республика Карачаево',
			68 => 'Республика Карелия',
			69 => 'Республика Коми',
			70 => 'Республика Крым',
			71 => 'Республика Марий Эл',
			72 => 'Республика Мордовия',
			73 => 'Республика Саха (Якутия)',
			74 => 'Республика Северная Осетия',
			75 => 'Республика Татарстан',
			76 => 'Республика Тыва (Тува)',
			77 => 'Республика Удмуртия',
			78 => 'Республика Хакасия',
			79 => 'Республика Чечня',
			80 => 'Республика Чувашия',
			81 => 'Еврейская АО',
			82 => 'Ненецкий АО',
			83 => 'Ханты-Мансийский АО',
			84 => 'Чукотский АО',
			85 => 'Ямало-Ненецкий АО',
		),
	),
);

/**
* @var array $base_country_city База городов по странам
*/
$base_country_city = array(
	//
	//
	// 	`7MM"""Mq.`7MMF'   `7MF'.M"""bgd  .M"""bgd `7MMF'      db
	// 	  MM   `MM. MM       M ,MI    "Y ,MI    "Y   MM       ;MM:
	// 	  MM   ,M9  MM       M `MMb.     `MMb.       MM      ,V^MM.
	// 	  MMmmdM9   MM       M   `YMMNq.   `YMMNq.   MM     ,M  `MM
	// 	  MM  YM.   MM       M .     `MM .     `MM   MM     AbmmmqMA
	// 	  MM   `Mb. YM.     ,M Mb     dM Mb     dM   MM    A'     VML
	// 	.JMML. .JMM. `bmmmmd"' P"Ybmmd"  P"Ybmmd"  .JMML..AMA.   .AMMA.
	//
	//
	1 => array(
		'Абакан' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 78,
		),
		'Азов' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Александров' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 18,
		),
		'Алексин' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Альметьевск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Анапа' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Ангарск' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Анжеро-Судженск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Апатиты' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 35,
		),
		'Арзамас' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Армавир' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Арсеньев' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 10,
		),
		'Артём' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 10,
		),
		'Архангельск' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 14,
		),
		'Асбест' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Астрахань' => array(
			'timezone' => 4,
			'federal_district' => 2,
			'region' => 15,
		),
		'Ачинск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Балаково' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 47,
		),
		'Балахна' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Балашиха' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Балашов' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 47,
		),
		'Барнаул' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 4,
		),
		'Батайск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Белгород' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 16,
		),
		'Белебей' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Белово' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Белогорск' => array(
			'timezone' => 9,
			'federal_district' => 4,
			'region' => 13,
		),
		'Белорецк' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Белореченск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Бердск' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 38,
		),
		'Березники' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Берёзовский' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Бийск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 4,
		),
		'Биробиджан' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 81,
		),
		'Благовещенск' => array(
			'timezone' => 9,
			'federal_district' => 4,
			'region' => 13,
		),
		'Бор' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Борисоглебск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 21,
		),
		'Боровичи' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 37,
		),
		'Братск' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Брянск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 17,
		),
		'Бугульма' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Будённовск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Бузулук' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 40,
		),
		'Буйнакск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Великие Луки' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 43,
		),
		'Великий Новгород' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 37,
		),
		'Верхняя Пышма' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Видное' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Владивосток' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 10,
		),
		'Владикавказ' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 74,
		),
		'Владимир' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 18,
		),
		'Волгоград' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 19,
		),
		'Волгодонск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Волжск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 71,
		),
		'Волжский' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 19,
		),
		'Вологда' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 20,
		),
		'Вольск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 47,
		),
		'Воркута' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 69,
		),
		'Воронеж' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 21,
		),
		'Воскресенск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Воткинск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 77,
		),
		'Всеволожск' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Выборг' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Выкса' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Вязьма' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 50,
		),
		'Гатчина' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Геленджик' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Георгиевск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Глазов' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 77,
		),
		'Горно-Алтайск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 4,
		),
		'Грозный' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 79,
		),
		'Губкин' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 16,
		),
		'Гудермес' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 79,
		),
		'Гуково' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Гусь-Хрустальный' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 18,
		),
		'Дербент' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Дзержинск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Димитровград' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 56,
		),
		'Дмитров' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Долгопрудный' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Домодедово' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Донской' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Дубна' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Егорьевск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Ейск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Екатеринбург' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Елабуга' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Елец' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 32,
		),
		'Ессентуки' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Железногорск (Курская область)' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 30,
		),
		'Железногорск (Красноярский край)' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Жигулёвск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Жуковский' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Заречный' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 42,
		),
		'Зеленогорск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Зеленодольск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Златоуст' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Иваново' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 22,
		),
		'Ивантеевка' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Ижевск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 77,
		),
		'Избербаш' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Иркутск' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Искитим' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 38,
		),
		'Ишим' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 55,
		),
		'Ишимбай' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Йошкар-Ола' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 71,
		),
		'Казань' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Калининград' => array(
			'timezone' => 2,
			'federal_district' => 3,
			'region' => 24,
		),
		'Калуга' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 25,
		),
		'Каменск-Уральский' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Каменск-Шахтинский' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Камышин' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 19,
		),
		'Канск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Каспийск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Кемерово' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Кинешма' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 22,
		),
		'Кириши' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Киров' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 27,
		),
		'Кирово-Чепецк' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 27,
		),
		'Киселёвск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Кисловодск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Клин' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Клинцы' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 17,
		),
		'Ковров' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 18,
		),
		'Когалым' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Коломна' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Комсомольск-на-Амуре' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 12,
		),
		'Копейск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Королёв' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Кострома' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 28,
		),
		'Котлас' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 14,
		),
		'Красногорск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Краснодар' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Краснокаменск' => array(
			'timezone' => 9,
			'federal_district' => 5,
			'region' => 5,
		),
		'Краснокамск' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Краснотурьинск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Красноярск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Кропоткин' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Крымск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Кстово' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Кузнецк' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 42,
		),
		'Кумертау' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Кунгур' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Курган' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 29,
		),
		'Курск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 30,
		),
		'Кызыл' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 76,
		),
		'Лабинск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Лениногорск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Ленинск-Кузнецкий' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Лесосибирск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Липецк' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 32,
		),
		'Лиски' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 21,
		),
		'Лобня' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Лысьва' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Лыткарино' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Люберцы' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Магадан' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 33,
		),
		'Магнитогорск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Майкоп' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 59,
		),
		'Махачкала' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Междуреченск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Мелеуз' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Миасс' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Минеральные Воды' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Минусинск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Михайловка' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 19,
		),
		'Михайловск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Мичуринск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 51,
		),
		'Москва' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 1,
		),
		'Мурманск' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 35,
		),
		'Муром' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 18,
		),
		'Мытищи' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Набережные Челны' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Назарово' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Назрань' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 64,
		),
		'Нальчик' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 65,
		),
		'Наро-Фоминск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Находка' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 10,
		),
		'Невинномысск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Нерюнгри' => array(
			'timezone' => 9,
			'federal_district' => 4,
			'region' => 73,
		),
		'Нефтекамск' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Нефтеюганск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Нижневартовск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Нижнекамск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Нижний Новгород' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Нижний Тагил' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Новоалтайск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 4,
		),
		'Новокузнецк' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Новокуйбышевск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Новомосковск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Новороссийск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Новосибирск' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 38,
		),
		'Новотроицк' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 40,
		),
		'Новоуральск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Новочебоксарск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 80,
		),
		'Новочеркасск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Новошахтинск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Новый Уренгой' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 85,
		),
		'Ногинск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Норильск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 8,
		),
		'Ноябрьск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 85,
		),
		'Нягань' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Обнинск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 25,
		),
		'Одинцово' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Озёрск' => array(
			'timezone' => 2,
			'federal_district' => 3,
			'region' => 24,
		),
		'Октябрьский' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Омск' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 39,
		),
		'Оренбург' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 40,
		),
		'Орехово-Зуево' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Орск' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 40,
		),
		'Орёл' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 41,
		),
		'Павлово' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Павловский Посад' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Пенза' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 42,
		),
		'Первоуральск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Пермь' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Петрозаводск' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 68,
		),
		'Петропавловск-Камчатский' => array(
			'timezone' => 12,
			'federal_district' => 4,
			'region' => 6,
		),
		'Подольск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Полевской' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Прокопьевск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Прохладный' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 65,
		),
		'Псков' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 43,
		),
		'Пушкино' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Пятигорск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Раменское' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Ревда' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Реутов' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Ржев' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 52,
		),
		'Рославль' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 50,
		),
		'Россошь' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 21,
		),
		'Ростов-на-Дону' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Рубцовск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 4,
		),
		'Рыбинск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 58,
		),
		'Рязань' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 45,
		),
		'Салават' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Сальск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Самара' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Санкт-Петербург' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 2,
		),
		'Саранск' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 72,
		),
		'Сарапул' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 77,
		),
		'Саратов' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 47,
		),
		'Саров' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 36,
		),
		'Свободный' => array(
			'timezone' => 9,
			'federal_district' => 4,
			'region' => 13,
		),
		'Северодвинск' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 14,
		),
		'Северск' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 53,
		),
		'Сергиев Посад' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Серов' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 49,
		),
		'Серпухов' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Сертолово' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Сибай' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Симферополь' => array(
			'timezone' => 3,
			'federal_district' => 9,
			'region' => 70,
		),
		'Славянск-на-Кубани' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Смоленск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 50,
		),
		'Соликамск' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Солнечногорск' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Сосновый Бор' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Сочи' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Ставрополь' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 11,
		),
		'Старый Оскол' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 16,
		),
		'Стерлитамак' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Ступино' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Сургут' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Сызрань' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Сыктывкар' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 69,
		),
		'Таганрог' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Тамбов' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 51,
		),
		'Тверь' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 52,
		),
		'Тимашёвск' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Тихвин' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 31,
		),
		'Тихорецк' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Тобольск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 55,
		),
		'Тольятти' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Томск' => array(
			'timezone' => 6,
			'federal_district' => 5,
			'region' => 53,
		),
		'Троицк' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Туапсе' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 7,
		),
		'Туймазы' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Тула' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Тюмень' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 55,
		),
		'Узловая' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Улан-Удэ' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 62,
		),
		'Ульяновск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 56,
		),
		'Урус-Мартан' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 79,
		),
		'Усолье-Сибирское' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Уссурийск' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 10,
		),
		'Усть-Илимск' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Уфа' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 61,
		),
		'Ухта' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 69,
		),
		'Фрязино' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Хабаровск' => array(
			'timezone' => 10,
			'federal_district' => 4,
			'region' => 12,
		),
		'Ханты-Мансийск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 83,
		),
		'Хасавюрт' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 63,
		),
		'Химки' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Чайковский' => array(
			'timezone' => 5,
			'federal_district' => 7,
			'region' => 9,
		),
		'Чапаевск' => array(
			'timezone' => 4,
			'federal_district' => 7,
			'region' => 46,
		),
		'Чебоксары' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 80,
		),
		'Челябинск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 57,
		),
		'Черемхово' => array(
			'timezone' => 8,
			'federal_district' => 5,
			'region' => 23,
		),
		'Череповец' => array(
			'timezone' => 3,
			'federal_district' => 3,
			'region' => 20,
		),
		'Черкесск' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 67,
		),
		'Черногорск' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 78,
		),
		'Чехов' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Чистополь' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 75,
		),
		'Чита' => array(
			'timezone' => 9,
			'federal_district' => 5,
			'region' => 5,
		),
		'Шадринск' => array(
			'timezone' => 5,
			'federal_district' => 6,
			'region' => 29,
		),
		'Шали' => array(
			'timezone' => 3,
			'federal_district' => 8,
			'region' => 79,
		),
		'Шахты' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 44,
		),
		'Шуя' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 22,
		),
		'Щёкино' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 54,
		),
		'Щёлково' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Электросталь' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 34,
		),
		'Элиста' => array(
			'timezone' => 3,
			'federal_district' => 2,
			'region' => 66,
		),
		'Энгельс' => array(
			'timezone' => 3,
			'federal_district' => 7,
			'region' => 47,
		),
		'Южно-Сахалинск' => array(
			'timezone' => 11,
			'federal_district' => 4,
			'region' => 48,
		),
		'Юрга' => array(
			'timezone' => 7,
			'federal_district' => 5,
			'region' => 26,
		),
		'Якутск' => array(
			'timezone' => 9,
			'federal_district' => 4,
			'region' => 73,
		),
		'Ярославль' => array(
			'timezone' => 3,
			'federal_district' => 1,
			'region' => 58,
		),
	),
);
?>